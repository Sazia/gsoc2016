# Guideline for student application 
## General guidelines

* Applications that do not contain at least the minimum information in the template **will not** get a slot.

* The best applications run to several pages (10+) if printed, and show considerable thought and planning.  The application process is longer this year than it has been in some prior years, so this should not be a problem.

* Be sure to follow the general guidelines available in the [GSoC student guide](http://write.flossmanuals.net/gsocstudentguide/writing-a-proposal/).

* If you have questions about details, contact your mentors first.  If you still have choices to make or different possible approaches, explore the most likely ones in your application. The goal of your application is to demonstrate your capability to complete the work of the project over the course of the summer, demonstrate that you've given a fair amount of thought to an implementation approach, and plan, as much as possible, for choices that may change the goals or timeline in the application.

* Once your draft application has been submitted, you may receive comments from other C3G mentors, either listed project mentors or others.  Please respond to the comments promptly and completely.  Being non-responsive before you've even been awarded a slot is not a good sign for the success of your proposal.

* Originality is always welcomed. If you have improvement ideas that go beyong the proposed project scope, by all means include them in your proposal.

### Format
The proposal should be submitted as a Markdown document, that you will make available from your public source control repository.

It should include the following sections, in this particular order:

* Project Info
* Biographical Information
* Contact Information
* Student Affiliation
* Schedule Conflicts
* Mentors
* Synopsis
* Benefits to Community
* Coding Plan & Methods
* Timeline
* Management of Coding Project
* Test


## Project Info

Project title: 

Project short title (30 characters): 

URL of project idea page: 

## Biographical Information

Provide a brief (text) biography, and why you think your background qualifies you for this project.

## Contact Information

Student name: 

Melange Link_id:  

Student postal address: 

Telephone(s): 

Email(s): 

Other communications channels: Skype/Google+, etc. : 


## Student Affiliation

Institution: 

Program: 

Stage of completion: 

Contact to verify: 


## Schedule Conflicts

Please list any schedule conflict that will interfere with you treating your proposed C3G GCoC project as a full time job in the summer.  If you are applying to other internships, or have other commitments, list them.

## Mentors

Mentor names: 

Mentor emails: 

Mentor link_ids: 

Have you been in touch with the mentors? When and how? 


## Synopsis (max 150 words)

Start your proposal with a short summary of the project, designed to convince the reviewer to read the rest of the proposal.
 

##  Benefits to Community (max 250 words)

Don't forget to make your case for a benefit to the organization, not just to yourself.  Why would Google and your organization be proud to sponsor this work? How would open source or society as a whole benefit? What cool things would be demonstrated?




## Coding Plan & Methods

Describe in detail your plan for completing the work.  What functions will be written, how and when will you do design, how will you verify the results of your coding?  Each project is different, please make your application appropriate to the work you propose.  

Describe perceived obstacles and challenges, and how you plan to overcome them.



## Timeline

(consult GSOC schedule)

Provide a detailed timeline of how you plan to spend your summer, organized by deliverables.  Don't leave testing and documentation for last, as that's almost a guarantee of a failed project. 

What is your contingency plan for things not going to schedule? 


## Management of Coding Project

How do you propose to ensure code is submitted / tested?

How often do you plan to commit?  What changes in commit behavior would indicate a problem?


## Test

Describe the qualification test that you have submitted to you project mentors.  If feasible, include code, details, output, and example of similar coding problems that you have solved.


## Anything Else

